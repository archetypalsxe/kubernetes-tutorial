# Kubernetes Tutorial
* https://kubernetes.io/docs/tutorials/hello-minikube/

## Running
* Bring up Kubernetes daemon: `minikube start`
* From the hellonode folder
  * `eval $(minikube docker-env)`
  * Build the docker image:
    * `docker build -t hello-node:v1 .`
  * `kubectl run hello-node --image=hello-node:v1 --port=8080 --image-pull-policy=Never`
  * Make the container accessible besides internally
    * `kubectl expose deployment hello-node --type=LoadBalancer`
  * Accessing:
    * `minikube service hello-node`
      * Just goes to port
    * Determining port:
      * `kubectl get services`
      * Access that port directly (Something like: `8080:31292/TCP` means port 31292)
      * Retrieving IP:
        * `minikube ip` (192.168.99.100)
  * Updating image in Minikube
    * `kubectl set image deployment/hello-node hello-node=hello-node:v2`
    * `kubectl set image deployment apache2php7-ssl apache2php7-ssl=partnercomm/apache2php7-ssl:latest --record`
      * Updating in EKS
    * Check status of update
      * `kubectl rollout status deployments/hello-node`
    * Revert update
      * `kubectl rollout undo deployments/hello-node`
    * Updating Deployment
      * `kubectl apply -f https://k8s.io/examples/application/deployment-update.yaml`
* Proxy
  * `kubectl proxy`
* Store pod name in variable:
  * `export POD_NAME=$(kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')`
* Access: curl http://localhost:8001/api/v1/namespaces/default/pods/$POD_NAME/proxy/

## Bringing Down
* `kubectl delete service hello-node`
* `kubectl delete deployment hello-node`
* `kubectl delete pod nginx`
* Optional
  * `minikube stop`
  * `eval $(minikube docker-env -u)`
  * `minikube delete`

## Commands
* Creating Pod from Yaml
  * `kubectl create -f simple-pod.yaml`
  * Creating BusyBox (Run commands remotely)
    * `kubectl run busybox --image=busybox --restart=Never --tty -i --generator=run-pod/v1 --env "POD_IP=$(kubectl get pod nginx -o go-template='{{.status.podIP}}')"`
* Scale deployment:
  * `kubectl scale deployments/hello-node --replicas=4`
* Run bash shell
  * `kubectl exec -ti $POD_NAME bash`
* List addons:
  * `minikube addons list`
* Enable addon:
  * `minikube addons enable heapster`
* Access addon:
  * `minikube addons open heapster`
* Images inside of pods and images used to build pods:
  * `kubectl describe pods`
* Get environmental variables (run command on pod)
  * `kubectl exec $POD_NAME env`
* Retrieving node port in use
  * `export NODE_PORT=$(kubectl get services/hello-node -o go-template='{{(index .spec.ports 0).nodePort}}')`
* Labeling a pod:
  * `kubectl label pod $POD_NAME app=v1`

## Monitoring:
* Adding `--watch` will make it poll and display changes as they occur
* `kubectl get deployments`
* `kubectl get pods -o wide`
  * `kubectl logs <POD-NAME>`
* `kubectl get events`
* `kubectl config view`
* `kubectl get services`
* `kubectl describe deployment`
